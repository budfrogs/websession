const express = require('express');
const session = require('express-session');
const cors = require('cors');
const os = require('os');

const app = express();
app.use(cors());

app.use(
  session({
    secret: 'my secret',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }, // for local development, secure should be true in production when using https
  })
);

app.use((req, res, next) => {
  if (!req.session.views) {
    req.session.views = 0;
  }

  req.session.views++;

  next();
});

app.get('/views', (req, res) => {
  res.json({ views: req.session.views });
});

app.get('/session', (req, res) => {
  res.json({ sessionID: req.session.id });
});

app.get('/ip', (req, res) => {
  const networkInterfaces = os.networkInterfaces();
  for (let networkInterface of Object.values(networkInterfaces)) {
    for (let addressInfo of networkInterface) {
      if (addressInfo.family === 'IPv4' && !addressInfo.internal) {
        res.json({
          serverIP: addressInfo.address,
          clientIP: req.socket.remoteAddress,
        });
      }
    }
  }
});

app.listen(3001, () => {
  console.log('Server started on http://localhost:3001');
});

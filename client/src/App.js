import axios from 'axios';
import { useEffect, useState } from 'react';

function App() {
  const [views, setViews] = useState(null);
  const [serverIP, setServerIP] = useState(null);
  const [clientIP, setClientIP] = useState(null);
  const [sessionID, setSessionID] = useState(null);
  
  useEffect(() => {
    axios
      .get('http://10.0.0.42:3001/views')
      .then((res) => setViews(res.data.views))
      .catch((err) => console.error(err));

    axios
      .get('http://10.0.0.42:3001/ip')
      .then((res) => {
        setServerIP(res.data.serverIP);
        setClientIP(res.data.clientIP);
      })
      .catch((err) => console.error(err));

   axios
      .get('http://10.0.0.42:3001/session')
      .then((res) => {
        setSessionID(res.data.sessionID);
      })
      .catch((err) => console.error(err));
 
  }, []);


  return (
    <div className='App'>
      <header className='App-header'>
        <p>Page views: {views}</p>
        <p>Server IP: {serverIP}</p>
        <p>Client IP: {clientIP}</p>
        <p>Session ID: {sessionID}</p>
      </header>
    </div>
  );
}

export default App;
